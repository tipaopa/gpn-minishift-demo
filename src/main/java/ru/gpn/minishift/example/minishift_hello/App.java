package ru.gpn.minishift.example.minishift_hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {

	@GetMapping("/say/hello")
	String sayHello() {
		return "Hello world!";
	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

}
